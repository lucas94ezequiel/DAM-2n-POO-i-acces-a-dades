package exemple_processat_xsl;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Conversor {
	public static void main(String[] args) {
		try (FileOutputStream escriptor = new FileOutputStream("alumnes.html")) {
			Source estils = new StreamSource("alumnes_plantilla.xsl"); //Fitxer processador XSL
			Source dades = new StreamSource("alumnes.xml");	//Fitxer origen
			Result resultat = new StreamResult(escriptor); //Fitxer destí
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer(estils);
			transformer.transform(dades, resultat);
		} catch (IOException | TransformerException e) {
			System.err.println(e.getMessage());
		}
	}
}
