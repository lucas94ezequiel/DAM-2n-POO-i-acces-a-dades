package cerca_binaria.c;

import java.util.ArrayList;

public class Comprova {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		comprovaLlistaOrdenada();
		comprovaLlistaNormal();
	}
	
	public static void comprovaLlistaOrdenada() {
		int i, j, k;
		long t0, t1;
		// Creem la llista
		System.out.println("S'està creant la llista...");
		LlistaOrdenada l = new LlistaOrdenada();
		// Posem els nombres en sentit invers pq resulta molt més ràpida tal i com està fet l'add
		for (j=9; j>=0; j--) {
			System.out.print(".");
			for (i=9999; i>=0; i--) {
				l.add(j*10000 + i);
			}
		}
		System.out.println(" fet");
		System.out.println("S'està testejant la velocitat...");
		
		// Temps inicial
		t0 = System.currentTimeMillis();
		for (j=0; j<10; j++) {
			System.out.print(".");
			k = j*10000;
			for (i=0; i<10000; i++) {
				l.binarySearch(k + i);
			}
		}
		// Temps final
		t1 = System.currentTimeMillis();
		// Escrivim el temps total
		System.out.println(" fet");
		System.out.println("S'ha tardat "+(t1 - t0)+" milisegons");
	}
	
	public static void comprovaLlistaNormal() {
		int i, j, k;
		long t0, t1;
		// Creem la llista
		System.out.println("S'està creant la llista...");
		ArrayList<Integer> l = new ArrayList<Integer>();
		// Posem els nombres en sentit invers pq ho he copiat de l'altre mètode ;)
		for (j=9; j>=0; j--) {
			System.out.print(".");
			for (i=9999; i>=0; i--) {
				l.add(j*10000 + i);
			}
		}
		System.out.println(" fet");
		System.out.println("S'està testejant la velocitat...");
		
		// Temps inicial
		t0 = System.currentTimeMillis();		
		for (j=0; j<10; j++) {
			System.out.print(".");
			k = j*10000;
			for (i=0; i<10000; i++) {
				l.indexOf(k + i);
			}
		}
		// Temps final
		t1 = System.currentTimeMillis();
		// Escrivim el temps total
		System.out.println(" fet");
		System.out.println("S'ha tardat "+(t1 - t0)+" milisegons");
	}
	
	/*
	 * Conclusions:
	 * 
	 * - Tot i que en casos concrets i en llistes molt petites, la cerca linial pot ser
	 * més ràpida que la cerca binària, en general, la cerca binària és molt més ràpida.
	 * - La cerca binària no es pot utilitzar sempre, perquè necessitem que la llista
	 * estigui ordenada, cosa que no passa en general.
	 * - L'ús de la cerca binària sobre una LinkedList seria molt més lent, perquè per
	 * testejar un element, internament caldria recòrrer tots els anteriors.
	 * - De mitjana, la cerca linial necessita n/2 operacions per trobar un element,
	 * mentre que la cerca binària necessita com a molt 1 + log en base 2 de n operacions.
	 * - En l'exemple, n = 100000. Per tant, la cerca iterativa necessita 50000 operacions
	 * i la binària en necessita entre 14 i 15.
	 * - La cerca linial ha resultat fins i tot una mica més ràpida del que havia de ser.
	 * Això es deu, probablement, al fet que el codi de la cerca està implementat en
	 * llenguatge C i compilat, mentre que el nostre codi s'ha d'interpretar.
	 */

}
