package cartabaralla;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Baralla {
	private static final Random random = new Random();
	private List<Carta> cartes = new ArrayList<Carta>(48);
	
	public Baralla() {
		int n, pal;
		Carta c;
		
		for (pal = Carta.OROS; pal <= Carta.BASTOS; pal++) {
			for (n = 1; n <= 12; n++) {
				c = new Carta(n, pal);
				cartes.add(c);
			}
		}
	}
	
	public void remena() {
		List<Carta> nova = new ArrayList<Carta>(cartes.size());
		int pos;
		
		for (Carta c : cartes) {
			pos = random.nextInt(nova.size()+1);
			nova.add(pos, c);
		}
		cartes = nova;
	}
	
	public Carta roba() throws IndexOutOfBoundsException {
		return cartes.remove(0);
	}
	
	public void eliminaNum(int num) {
		Iterator<Carta> it = cartes.iterator();
		Carta c;
		
		while (it.hasNext()) {
			c = it.next();
			if (c.getNum() == num)
				it.remove();
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for (Carta c : cartes) {
			s += c.toString() + " ";
		}
		return s;
	}
}
