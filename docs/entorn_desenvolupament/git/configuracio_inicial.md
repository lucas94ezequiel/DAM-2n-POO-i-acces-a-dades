## Configuració inicial del git ##

- Creem un usuari a GitLab, cerquem el projecte original i
fem un fork. Això ens crearà una nova còpia del projecte al
nostre espai personal, on tindrem permís per fer qualsevol
modificació.

- A l'ordinador, creem una carpeta on treballar.
Des de la línia d'ordres ens situem dins d'aquesta carpeta
i clonem el repositori.

Aquest serà el directori de treball habitual i per tant cal
que es faci a un espai editable (no congelat).

Hem de clonar el nostre fork del repositori, no el repositori
original:

```
$ mkdir curs
$ cd curs
$ git clone https://gitlab.com/<usuari>/DAM-2n-POO-i-acces-a-dades.git
```

- Configurem el nostre nom d'usuari i email:

```
$ cd DAM-2n-POO-i-acces-a-dades
$ git config --local user.email <adreça correu>
$ git config --local user.name <nom que ha de constar als registres>
```

- Evitem que ens surti un missatge d'avís cada cop que fem
un push, indicant el sistema per defecte que ha d'utilitzar:

```
$ git config --local push.default simple
```

- Fem que després de demanar les credencial d'usuari les
guardi en memòria una estona, per evitar haver de repetir-les
massa sovint:

```
$ git config --local credential.helper cache
```

- Configurem l'editor que volem utilitzar per editar els missatges de commit:

```
$ git config --local core.editor "atom --wait"
```

Això posaria l'editor Atom com a editor per defecte. Si l'executable de
l'editor que volem no és al *path* del sistema, caldrà indicar la ruta
completa.

- Afegim el repositori original com un remot més del nostre
projecte:

```
$ git remote add upstream https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades.git
```

Per veure la configuració actual dels repositoris remots
podem utilitzar:

```
$ git remote -v
```
